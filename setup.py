from setuptools import setup

setup(
    name='pyspacio',
    version='1.22',
    packages=['pyspacio'],
    license='',
    author='Antonio-Carlos Padoan',
    description='Wrapper around Lisp library spacio'
)
