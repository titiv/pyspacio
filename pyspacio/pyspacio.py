import cl4py
from opcua import Client
# from opcua.common.node import Node
# import datetime
import time as ptime
# from datetime import timezone
# from opcua.ua.uatypes import DataValue
import os
import zmq
import threading
from pydispatch import dispatcher
import re

SIGNAL_DOBJ = 'data-object-changed'


def _fix_false(r):
    if r == ():
        return False
    if type(r) == cl4py.Keyword:
        is_mms = r.name.startswith('IED-ERROR')
        is_mms_ok = r.name.startswith('IED-ERROR-OK')
        if is_mms and not(is_mms_ok):
            raise Exception(r.name)
        elif is_mms_ok:
            return True
    if type(r) == tuple:
        ll = list()
        for i in r:
            if isinstance(i, int):
                ll.append(i)
            elif i == ():
                ll.append(False)
            elif i is True:
                ll.append(True)
            else:
                ll.append((i.car, i.cdr))
        return ll
    return r


class IecSession():
    def __init__(self):
        self._lisp = cl4py.Lisp()
        self._req = self._lisp.function('require')
        self._req('asdf')
        self._load = self._lisp.function('load')
        self._load_system = self._lisp.function('asdf:load-system')
        self._load_system('lispiec')
        self._rspace = self._lisp.find_package("rspace")
        self._pano = self._lisp.find_package("pano")
        self._iec = self._lisp.find_package("iec")
        self._alias = self._lisp.function('lispiec::alias')
        self._bay = self._lisp.function('lispiec::bay')
        self._srv_is_run = self._lisp.function('lispiec::iec-server-is-running')
        self._stop_zmq = self._lisp.function('lispiec-rspace::stop-zmq')
        self._get_reference = self._lisp.function('lispiec::obj-reference-str')
        self._get_ips = self._lisp.function('lispiec::site-list-ieds-ip')
        self._list_toggle = self._iec.list_dataobjects_toggle
        self._list_set_on = self._iec.list_dataobjects_set_on
        self._list_set_off = self._iec.list_dataobjects_set_off
        self._list_get_values = self._iec.list_dataobjects_get_value
        self._find_bay_long_label = self._lisp.function('lispiec::site-find-bay-longlabel')
        self._zcontext = zmq.Context.instance()
        self._zsocket = self._zcontext.socket(zmq.SUB)
        self._zsocket.setsockopt(zmq.SUBSCRIBE, b'')
        self._tcp_zmq = 5557
        self._start_web = self._lisp.function('lispiec-rspace:start-web')
        self._zrun = False
        # List of lisp functions to be used :
        self._site = None
        self._last_goose_rec = None
        # self._dispatch_signal = 'change'
        # self._cdcs_created = list()
        dispatcher.connect(self._print_event,
                           sender=dispatcher.Any,
                           signal=SIGNAL_DOBJ)

    def session(self):
        return self._lisp

    def make_site(self,
                  project_path='~/ieds/',
                  tcp_port=10000,
                  tcp_zmq=5557,
                  ds_regex='(GSE|DQCE|DQPO|CYPO|DQGW)$'):
        if os.path.isdir(os.path.expanduser(project_path)):
            self._rspace.make_rspace(os.path.expanduser(project_path),
                                     tcp_port,
                                     ds_regex)
            self._site = self._rspace.rspace
            self._tcp_zmq = tcp_zmq

    def load_start_lisp_script(self, script_path='~/start_rspace.lisp'):
        if os.path.isfile(os.path.expanduser(script_path)):
            self._load(os.path.expanduser(script_path))
        self._rspace.start_zmq(self._tcp_zmq)

    def load_other_lisp_script(self, script_path):
        if os.path.isfile(os.path.expanduser(script_path)):
            self._load(os.path.expanduser(script_path))

    def start_ieds(self):
        self._iec.site_start_ieds(self._site)
        self._rspace.start_zmq(self._tcp_zmq)

    def start_goose(self, device_str):
        self._iec.site_start_ieds_goose(self._site, device_str)

    def stop_goose(self):
        self._iec.site_stop_ieds_goose(self._site)

    def destroy_site(self):
        self.stop_listen_events()
        self._stop_zmq
        self.stop_goose()
        self._lisp.process.terminate()

    def _zlistening(self):
        print("threading started.")
        while(self._zrun):
            msg = self._zsocket.recv()
            # print("zmq: ",  msg.decode())
            dispatcher.send(signal=SIGNAL_DOBJ,
                            sender=dispatcher.Any,
                            event=msg.decode())
            # myobj = self._cdcs_created_get_by_name(msg.decode())
            # if myobj is None:
            #     print('zmq: data object still not created in python.')
            # else:
            #     th = threading.Thread(
            #         target=myobj._default_handler_pointer(myobj))
            #     th.start()
            # ptime.sleep(0.005)

    def start_web(self, port=8080):
        self._start_web(port)

    def stop_listen_events(self):
        if self._zrun is True:
            self._zrun = False
            self._zsocket.disconnect(self._ztcp)

    def start_listen_events(self):
        if self._zrun is False:
            self._ztcp = 'tcp://127.0.0.1:'+str(self._tcp_zmq)
            self._zsocket.connect(self._ztcp)
            self._zrun = True
            f = threading.Thread(target=self._zlistening)
            f.start()

    def _cdcs_created_get_by_name(self, name):
        for dd in self._cdcs_created:
            if dd.iec_ref == name:
                return dd
        return None

    def _print_event(self, event=None):
        # print("Signal received: " + event)
        pass

    def get_list_dobjs_by_regex(self, ld_regex, ln_regex, dobj_regex):
        mylist = self._iec.site_get_dataobjects_by_regex(self._site,
                                                         ld_regex,
                                                         ln_regex,
                                                         dobj_regex)
        return CdcList(self, mylist)

    def get_list_dobjs_by_alias(self, alias_regex):
        mylist = self._iec.site_get_dataobjects_by_alias(self._site,
                                                         alias_regex)
        if mylist == ():
            return None
        return CdcList(self, mylist)

    def get_dobj_by_alias(self, alias_regex, bay):
        d = self._iec.site_get_dataobject_by_alias(self._site,
                                                   alias_regex,
                                                   bay)
        if d == ():
            return None
        cdc = Cdc(self,
                  "",
                  "",
                  "",
                  self._get_reference(d))
        if cdc == ():
            return None
        return cdc

    def get_dobj_by_ref(self, reference_str):
        d = self._iec.site_get_dataobject_by_ref(self._site, reference_str)
        if d == ():
            return None
        cdc = Cdc(self,
                  "",
                  "",
                  "",
                  self._get_reference(d))
        if cdc == ():
            return None
        return cdc

    def get_dobj_by_regex(self, ld_regex, ln_regex, do_regex):
        d = self._iec.site_get_dataobject_by_regex(self._site,
                                                   ld_regex,
                                                   ln_regex,
                                                   do_regex)
        if d == ():
            return None
        cdc = Cdc(self,
                  "",
                  "",
                  "",
                  self._get_reference(d))
        if cdc == ():
            return None
        return cdc

    def get_list_dobjs_by_regex_same_bay(self, dobj_origin,
                                         ld_regex,
                                         ln_regex,
                                         dobj_regex):
        mylist = self._iec.get_cdcs_same_bay(dobj_origin._dobj,
                                             ld_regex,
                                             ln_regex,
                                             dobj_regex,
                                             self._site)
        return CdcList(self, mylist)

    def get_ieds_running(self):
        mylist = self._iec.site_list_ieds_running(self._site)
        return IedList(self, mylist).show_list()

    def get_ieds_port(self):
        mylist = self._iec.site_list_ieds_port(self._site)
        return IedList(self, mylist).show_list()

    def get_ieds_ip(self):
        mylist = self._get_ips(self._site)
        return IedList(self, mylist).show_list()

    def get_ied_by_name(self, name):
        return Ied(self, name)

    def get_bay_longlabel(self, bay_short_name):
        return self._find_bay_long_label(self._site, bay_short_name)

    def as_client_make_goose_receiver(self, device_name_str):
        r = self._iec.make_site_client_goose_receiver(self._site,
                                                      device_name_str)
        self._last_goose_rec = r
        return r

    def as_client_listen_goose_receiver(self):
        self._iec.site_client_goose_receivers_start_listening(self._site)

    def as_client_stop_listen_goose_receiver(self):
        self._iec.site_client_goose_receivers_stop_listening(self._site)


_regex_pano_ld_path = re.compile("^(.+)/[\\w\\.]+$")


class Cdc():
    def __init__(self, iecsession, ld_regex, ln_regex, dobj_regex,
                 optional_iec_ref=""):
        self._site = iecsession._site
        self._session = iecsession
        self._rspace = iecsession._rspace
        self._iec = iecsession._iec
        self._pano = iecsession._pano
        self._type_of = iecsession._lisp.function('type-of')
        if optional_iec_ref == "":
            self._dobj = self._iec.site_get_dataobject_by_regex(self._site,
                                                                ld_regex,
                                                                ln_regex,
                                                                dobj_regex)
        else:
            self._dobj = self._iec.site_get_dataobject_by_ref(self._site,
                                                              optional_iec_ref)
        self._toggle = self._iec.cdc_toggle
        self._on = self._iec.cdc_set_on
        self._off = self._iec.cdc_set_off
        self._get_value = self._iec.cdc_get_value
        self._get_timestamp = self._iec.cdc_get_timestamp
        self._set_value = self._iec.cdc_set_value
        self._set_value_act = self._iec.cdc_set_act_value
        self._set_value_lpl_valrev = self._iec.lpl_set_valrev
        self._set_invalid = self._iec.cdc_set_invalid
        self._set_valid = self._iec.cdc_set_valid
        self.pano_address = self._pano.get_pano_address(self._dobj)
        self._alias = iecsession._alias
        self._iec_ref = iecsession._lisp.function('lispiec::obj-reference-str')
        self._cdc_unlock = iecsession._lisp.function('lispiec::cdc-unlock')
        self._bay = iecsession._lisp.function('lispiec::bay')
        self._oper = iecsession._lisp.function('lispiec::cdc-client-operate')
        self._write = iecsession._lisp.function('lispiec::cdc-client-write-value')
        self.iec_ref = self._iec_ref(self._dobj)
        self.alias = self._alias(self._dobj)
        self.bay = self._bay(self._dobj)
        self.pano_ld_address = _regex_pano_ld_path.match(
            self.pano_address).group(1)
        # iecsession._cdcs_created.append(self)
        # self._default_handler_pointer = self._default_handler

    def toggle(self):
        self._toggle(self._dobj)
        return self.get_value()

    def set_on(self):
        r = self._on(self._dobj)
        return _fix_false(r)

    def set_off(self):
        r = self._off(self._dobj)
        return _fix_false(r)

    def set_invalid(self):
        self._set_invalid(self._dobj)

    def set_valid(self):
        self._set_valid(self._dobj)

    def get_value(self):
        r = self._get_value(self._dobj)
        return _fix_false(r)

    def get_validity(self):
        r = self._iec.cdc_get_validity(self._dobj)
        return _fix_false(r)

    def get_ctlmodel(self):
        r = self._iec.cdc_get_ctlmodel(self._dobj)
        return _fix_false(r)

    def set_value(self, value):
        r = self._set_value(self._dobj, value)
        return _fix_false(r)

    def get_cdc(self):
        return self._type_of(self._dobj).name

    def unlock_cdc(self):
        self._cdc_unlock(self._dobj)

    def lock_oper(self):
        if self.get_cdc() == 'CDC-DPC':
            self._session._lisp.eval(('setf', (cl4py.Symbol('ENACLOSE', 'LISPIEC'), self._dobj), 'nil'))
            self._session._lisp.eval(('setf', (cl4py.Symbol('ENAOPEN', 'LISPIEC'), self._dobj), 'nil'))

    def unlock_oper(self):
        if self.get_cdc() == 'CDC-DPC':
            self._session._lisp.eval(('setf', (cl4py.Symbol('ENACLOSE', 'LISPIEC') , self._dobj), 't'))
            self._session._lisp.eval(('setf', (cl4py.Symbol('ENAOPEN', 'LISPIEC'), self._dobj), 't'))

    def get_ied(self):
        r = self._iec.iec_server(self._dobj)
        return Ied(self._session,
                   self._iec.iec_server_get_name(r))

    def set_value_act(self,
                      general=False,
                      phsa=False,
                      phsb=False,
                      phsc=False,
                      neut=False):
        cdc = self.get_cdc()
        if cdc == 'CDC-ACT':
            self._set_value_act(self._dobj, general, phsa, phsb, phsc, neut)
            return self.get_value()

    def set_value_lpl_valrev(self,
                             valrev):
        self._set_value_lpl_valrev(self._dobj, valrev)
        self.get_value()

    def get_timestamp(self):
        return self._get_timestamp(self._dobj)

    def as_client_read_value(self, da_str):
        r = self._iec.cdc_client_read_value(self._dobj, da_str)
        return _fix_false(r)

    def as_client_operate(self, ctlval, orcat=2):
        return _fix_false(self._oper(self._dobj, ctlval, orcat))

    def as_client_operate_get_last_results(self):
        success = self._iec.cdc_client_get_last_success(self._dobj)
        addcause = self._iec.cdc_client_get_last_addcause(self._dobj)
        term_t = self._iec.cdc_client_get_last_term_t(self._dobj)
        oper_t = self._iec.cdc_client_get_last_oper_t(self._dobj)
        return {"success": success, "addcause": addcause.name,
                "term_t": term_t, "oper_t": oper_t}

    def as_client_write_value(self, value, da_str):
        self._write(self._dobj, value, da_str)


class OpcSession():
    def __init__(self, tcp_address):
        self._address = tcp_address
        self._client = Client(tcp_address)

    def connect(self):
        r = self._client.connect()
        return _fix_false(r)

    def disconnect(self):
        self._client.disconnect()

    def get_node(self, address):
        return self._client.get_node(address)

    def get_client(self, address):
        return self._client

    def get_root(self):
        return self._client.get_root_node()

    def get_site(self):
        self.get_node('ns=2;s=/Application/Site')

    def get_systeme(self):
        self.get_node('ns=2;s=/Application/Systeme')


class CdcHandler():
    def __init__(self, session, ld_regex, ln_regex, do_regex):
        self._session = session
        self._ld_regex = re.compile(ld_regex)
        self._ln_regex = re.compile(ln_regex)
        self._do_regex = re.compile(do_regex)
        self._split_regex = re.compile('(\\w+)/(\\w+)\\.(\\w+)')
        dispatcher.connect(self._print_event,
                           sender=dispatcher.Any,
                           signal=SIGNAL_DOBJ)
        self.dobj = None

    def _filter(self, message):
        m = self._split_regex.match(message)
        do_ok = self._do_regex.search(m.group(3))
        if do_ok is None:
            return False
        ln_ok = self._ln_regex.search(m.group(2))
        if ln_ok is None:
            return False
        ld_ok = self._ld_regex.search(m.group(1))
        if ld_ok is None:
            return False
        return True

    def _print_event(self, event=None):
        if(self._filter(event)):
            print("Handler called for DO: " + event)
            self.dobj = Cdc(self._session, "", "", "", optional_iec_ref=event)
            self._def_handler(self.dobj)

    def _def_handler(self, dobj=None):
        pass

    def set_handler(self, handler_function):
        self._def_handler = handler_function


class GenList():
    def __init__(self, session, lisp_list):
        self._list = lisp_list
        self._session = session

    def show_list(self):
        return self._listfy2(self._list)

    def _listfy(self, mylisp_list):
        mylist = list()
        icar = mylisp_list.car
        icdr = mylisp_list.cdr
        mylist.append(icar)
        while icdr != ():
            icar = icdr.car
            icdr = icdr.cdr
            mylist.append(icar)
        return mylist

    def _listfy2(self, mylisp_list):
        mylist = self._listfy(mylisp_list)
        mylist2 = list()
        for i in mylist:
            mylist2.append((i.car, _fix_false(i.cdr)))
        return mylist2

    def _sort_crit_2(self, item):
        item[1]

    def _show_list_sort(self):
        self.show_list().sort(key=self._sort_crit_2)

    def len(self):
        f = self._session._lisp.function("length")
        return f(self._list)

    def _pop(self):
        r = self._list.car
        self._list = self._list.cdr
        return r

    def remove(self, nth):
        ff = self._session._lisp.function("remove-if")
        tt = self._session._lisp.function("constantly")
        self._list = ff(tt(True),
                        self._list,
                        start=nth,
                        count=1)


class IedList(GenList):
    def __init__(self, session, lisp_list):
        GenList.__init__(self, session, lisp_list)

    def pop(self):
        return self._pop()


class CdcList(GenList):
    def __init__(self, session, lisp_list):
        GenList.__init__(self, session, lisp_list)

    def get_values(self):
        mylist = self._session._list_get_values(self._list)
        return self._listfy2(mylist)

    def show_bays(self):
        return self._listfybay(self._list)

    def show_nth(self):
        base_list = self.show_bays()
        mylist = list()
        n = 0
        for i in base_list:
            mylist.append((n, i))
            n += 1
        return mylist

    def _listfybay(self, mylisp_list):
        mylist = self._listfy(mylisp_list)
        mylist2 = list()
        for i in mylist:
            mylist2.append((i.car, self._session._bay(i.cdr)))
        return mylist2

    def toggle(self):
        mylist = self._session._list_toggle(self._list)
        return self._listfy2(mylist)

    def set_on(self):
        mylist = self._session._list_set_on(self._list)
        return self._listfy2(mylist)

    def set_off(self):
        mylist = self._session._list_set_off(self._list)
        return self._listfy2(mylist)

    def get_validity(self):
        lls = self.get_values()
        rrs = list()
        n = self.len()
        for i in range(n):
            d = self.get_nth_dobj(i)
            rrs.append((lls[i][0], d.get_validity()))
        return rrs

    def set_invalid(self):
        n = self.len()
        for i in range(n):
            d = self.get_nth_dobj(i)
            d.set_invalid()
        return self.get_validity()

    def set_valid(self):
        n = self.len()
        for i in range(n):
            d = self.get_nth_dobj(i)
            d.set_valid()
        return self.get_validity()

    def get_nth_dobj(self, n=0):
        mylist = self.show_list()
        if n > (len(mylist) - 1):
            return None
        d = mylist[n][1]
        if d == ():
            return None
        ref = self._session._get_reference(d)
        cdc = self._session.get_dobj_by_ref(ref)
        if cdc is None:
            return None
        return cdc

    def pop(self):
        r = self.get_nth_dobj(0)
        self._pop()
        return r

    def remove(self, nth):
        r = self.get_nth_dobj(nth)
        GenList.remove(nth)
        return r


class Ied():
    def __init__(self, session, name):
        site = session._site
        self._session = session
        self._is_running = session._srv_is_run
        self._ied = session._iec.site_get_ied_by_name(site, name)
        self._iec = session._iec
        self._func_connection = session._lisp.function('lispiec::iec-server-client-create-connection')
        self._func_close_connection = session._lisp.function('lispiec::iec-server-client-close-connection')
        self._func_connection_test = session._lisp.function('lispiec::iec-server-client-test')
        self._func_start_goose = session._lisp.function('lispiec::iec-server-start-goose-on-interface')
        self._func_stop_goose = session._lisp.function('lispiec::iec-server-stop-goose-on-interface')

    def start(self):
        self._iec.iec_server_start(self._ied)

    def stop(self):
        self._iec.iec_server_stop(self._ied)

    def is_running(self):
        running = self._is_running(self._ied)
        if running:
            return True
        else:
            return False

    def disable(self):
        self._iec.iec_server_disable(self._ied)

    def as_client_connect(self, use_local_address=False):
        r = self._func_connection(self._ied, use_local_address)
        return _fix_false(r)

    def as_client_close_connection(self):
        self._func_close_connection(self._ied)

    def as_client_read_value(self,
                             da_str):
        r = self._iec.iec_server_client_read_value_from_ref(self._ied,
                                                            da_str)
        return _fix_false(r)

    def as_client_write_value(self,
                              value,
                              da_str):
        r = self._iec.iec_server_client_write_value(self._ied,
                                                    value,
                                                    da_str)
        return _fix_false(r)

    def set_file_server_base_path(self, path):
        self._iec.iec_server_set_filebasepath(self._ied, path)

    def create_comtrade_files(self):
        self._iec.iec_server_create_sample_file(self._ied)

    def start_goose(self, device_str):
        self._func_start_goose(self._ied, device_str)

    def stop_goose(self):
        self._func_stop_goose(self._ied)

    def get_name(self):
        return self._iec.iec_server_get_name(self._ied)

    def get_name_opc_connected(self):
        return 'ns=2;s=/Application/Systeme/' + self.get_name() + '/IED/IEDConnecte.Value'

    def as_client_supervise_goose(self, receiver=None, gocb_name_reg='GS[IE]'):
        if receiver is None:
            receiver = self._session._last_goose_rec
        if receiver is not None:
            self._session._lisp.eval(cl4py.List(cl4py.Symbol('IEC-SERVER-CLIENT-SUPERVISE-GOOSES',
                                                             'LISPIEC'),
                                                self._ied,
                                                receiver,
                                                gocb_name_reg))

if __name__ == '__main__':
    session = IecSession()
    session.make_site("~/ieds/")
    session.load_start_lisp_script()
    mydj1 = Cdc(session,
                "BCU1LDCMDDJ",
                "CSWI1",
                "Pos")
    mydj2 = Cdc(session,
                "BCU1LDCMDDJ",
                "CSWI2",
                "Pos")
    h1 = CdcHandler(session, "1BCU1LDCMDDJ", "CSWI1", "Pos")

    def fh1(dobj):
        print(dobj.pano_address)
        mydj2.toggle()

    h1.set_handler(fh1)
    session.start_listen_events()
    # ll = session.get_list_dobjs_by_regex("\\w+ULIGNE\\w+LDCMDDJ$",
    #                                      "CSWI",
    #                                      "Pos")
