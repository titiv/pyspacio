import cl4py
import time as ptime


class SqlSession():
    def __init__(self):
        self._lisp = cl4py.Lisp()
        self._req = self._lisp.function('require')
        self._req('asdf')
        self._load = self._lisp.function('load')
        self._load_system = self._lisp.function('asdf:load-system')
        self._load_system('mssql')
        self._mssql = self._lisp.find_package('MSSQL')
        self._connection = None

    def connect(self, password, user="sa", db="RSPACE", ip='127.0.0.1'):
        ll = cl4py.List
        s = cl4py.Symbol
        k = cl4py.Keyword
        e = self._lisp.eval
        self._connection = e(ll(s('CONNECT', 'MSSQL'),
                                db,
                                user,
                                password,
                                ip,
                                k("EXTERNAL-FORMAT"),
                                k("UTF-8B")))

    def _listfy(self, mylisp_list):
        mylist = list()
        icar = mylisp_list.car
        icdr = mylisp_list.cdr
        mylist.append(icar)
        while icdr != ():
            icar = icdr.car
            icdr = icdr.cdr
            mylist.append(icar)
        return mylist

    def _listfy2(self, mylisp_list):
        if mylisp_list == ():
            return None
        mylist = self._listfy(mylisp_list)
        mylist2 = list()
        for i in mylist:
            ii = self._listfy(i)
            mylist2.append(ii)
        return mylist2

    def query(self, request_str):
        r = self._mssql.query(request_str,
                              connection=self._connection)
        return self._listfy2(r)

    def disconnect(self):
        self._mssql.disconnect(self._connection)


if __name__ == '__main__':
    sql = SqlSession()
    # sql.connect("R5p4ce@@@")
